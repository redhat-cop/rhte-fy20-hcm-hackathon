![lock](img/lock.png)

# Catalog

# How Do I Access the Application/Environment?

Please ask any of your hackathon leaders for the credentials to access the environment.

# How Do I Access the API? 

You need to have a valid username and password to access `cloud.redhat.com`, and the user must have the correct entitlements attached. 

To get your credentials to continue working on this project after the hackathon, please contact [Victor Estival](mailto:vestival@redhat.com), [John Hardy](jhardy@redhat.com), or [Steve Fulmer](sfulmer@redhat.com).

Once you have validated your user, you can access the API at
[https://cloud.redhat.com/api/catalog/v1.0/](https://cloud.redhat.com/api/catalog/v1.0/).

# API Documentation

The API is documented using [Open API](https://www.openapis.org/). You can find the full documentation [here](https://cloud.redhat.com/api/catalog/v1.0/openapi.json).



# API Clients
We expect most users to build a client. We maintain repositories for the clients that dev uses, ([created by Open API Generator](https://openapi-generator.tech/)):

* [https://github.com/ManageIQ/catalog-api-jsclient](https://github.com/ManageIQ/catalog-api-jsclient) 
* [https://github.com/ManageIQ/approval-api-client-ruby](https://github.com/ManageIQ/approval-api-client-ruby)
* [https://github.com/ManageIQ/approval-api-jsclient](https://github.com/ManageIQ/approval-api-jsclient)

# Tracks
[Approval API](track1.md)