# Extend CloudForms Capabilities via Ansible Playbooks

Ansible and CloudForms integration is getting very popular and we would like to explore what can we achieve by using CF and Ansible, either Ansible Automation Inside or Ansible Tower and provide / create / curate some content, best practices about its integration.

## Ansible Playbooks to manage Windows Systems

- create custom buttons assigned to a VM which allows end users to create and edit local user accounts

- create custom buttons assigned to a VM which allows end user to do configuration changes in the Registry

## Automate OpenStack Using CloudForms, Heat Template and Ansible

This work has been started during the CloudForms Hackathon. The purpose is to add the missing functionality to fully manage OpenStack with CloudForms.

CRUD projects, flavors, images, keypairs...

### Motivation

Telco Operators using OpenStack usually have multiple tools to manage their NFV setups. Positioning CloudForms to be the single pane of glass to integrated with not only Openstack, but also with MANO that is the management tool.

### Use-Case

NFV customers have a tool called the MANO, It consists of management tool to deploy business instances and drive day-2 operations of the applications, however there are doing a lot of manual things before hand on the infrastructure side. So currently using Horizon namely to CRUD:

- Create Tenants aka project
- Create networks
- attach flavors
- Security groups

In the same time we might need to add Day-2 operations from Infrastructure perspective :

- Attach new VIP.
- Create and attach a disks volumes
- LB...

As an extension, there are also some VNFs which do NOT integrate with the MANO, so starting and stopping those instances is 100% manual in best case using scripts...  In this context, I want to position Cloudforms as well with Ansible to manage those VNFs (VNF can be one or more instances).
All Contributions have started [in this repository](https://gitlab.com/idhaoui/automate-osp-with-heat-and-ansible/tree/master) ... Feel free to contribute

## Operational Tasks

Custom Buttons allow end users to perform actions on a specific object without having the necessary credentials or understanding the low level technology. This has been used to build Playbooks and buttons which allows users to

- install packages

- run operational commands (e.g. restart a service, wipe the yum cache, ...)

This playbooks can be found in the [Ansible](https://gitlab.com/redhat-cop/mbu-lab/ansible) Repository of the MBU Lab.

We would like to see more real world examples, not limited to VM management.

## Ken 

Ken has started his repo [here](https://gitlab.com/ksugawar/apac-rhte-hcm-hackathon-trk3.git)
