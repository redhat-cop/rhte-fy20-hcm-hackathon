![coding](img/coding.jpg)

# Hybrid Cloud Management Hackathon

No matter what your role is at Red Hat®, you can benefit from hands-on experience with Red Hat Hybrid Cloud Management. During this session, you have the opportunity to explore Cost Management, Catalog, and CloudForms®. You learn how to use APIs to generate outputs, how to use Scripting for Reporting, and how to extend Red Hat CloudForms with Red Hat Ansible® Automation. The goal is to push you to your limits exploring APIs, new features, and capabilities. At the same time, you gather concrete data on the new capabilities of the Hybrid Cloud Management suite of products. 


Thanks for joining this hackathon. If you want to know more about what a hackathon is, please refer to the presentation [here](https://docs.google.com/presentation/d/1AdZzq92lGTcd9uqv6_g0uPnr70vYFbcr08VRhT3Tr3U/edit#slide=id.g5c7ba2e508_0_200).

## Important!

 ***Please note that we will use the same repository and tracks in all three regions: APAC, EMEA and NA. The goal is to continue the hackathon throughout the RHTE***.

# Official Hackathon Definition

A hackathon (also known as a hack day, hackfest, or codefest) is a design sprint-like event in which computer programmers and others involved in software development, including graphic designers, interface designers, project managers, and others--often including subject-matter experts--collaborate intensively on software projects.

Source: https://en.wikipedia.org/wiki/Hackathon

# Hybrid Cloud Management Hackathon Definition

The Hybrid Cloud Management Hackathon is an event spanning a few hours in which product experts from all Red Hat departments and Partners collaborate on a Hybrid Cloud Management solution that includes CloudForms, Cost Management, and Catalog to create API examples, Automate Code, Ansible Playbooks, Reports, Policies, Demos, Guides, Blog Posts, or any other content related to the product.

Source: Christian Jung/COPs


# What to Expect

You can expect to expand and explore current product capabilities. You can enroll in any of the tracks. Due to the short amount of time we have, we do not think it is a good idea to swap tracks, but you can if you want.

# What should I deliver?
We do expect you to contribute with code, feedback, ideas, pull request... the limit is your imagination!

# Generic API Information About cloud.redhat.com
You can find generic information about the available API services at https://cloud.redhat.com/api/.

# Tools
You need to provide all output to GitLab. You can use any other tool, such as Google docs or GitHub, but you must add a link to your progress into this GitLab repository: https://gitlab.com/redhat-cop/rhte-fy20-hcm-hackathon.

# Where Can I Find the Tracks?
You can find the tracks inside each product's folder:

[CloudForms](CloudForms/README.md)

[Cost Management](Cost Management/README.md)

[Catalog](Catalog/README.md)

Image by <a href="https://pixabay.com/users/Pexels-2286921/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1841550">Pexels</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1841550">Pixabay</a>

Sortlink to this page: http://bit.ly/rhte19hack
